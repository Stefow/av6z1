﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kalkulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double broj_prvi=0,broj_drugi = 0, rezultat=0;
        string zadnja_funk = "a";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void oduzmi_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
            }
            else
            {
                double.TryParse(Broj1.Text, out broj_prvi);
                double.TryParse(Broj2.Text, out broj_drugi);
                if (broj_prvi != 0)
                {
                    Broj1.Text = (broj_prvi - broj_drugi).ToString();
                }
                else Broj1.Text = (broj_drugi).ToString();

                Broj2.Text = "0";
                zadnja_funk = "-";
            }
            funk.Text = "-";
        }

        private void Pomnozi_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
            }
            else
            {
                double.TryParse(Broj1.Text, out broj_prvi);
                double.TryParse(Broj2.Text, out broj_drugi);
                if (broj_prvi != 0)
                {
                    Broj1.Text = (broj_prvi * broj_drugi).ToString();
                }
                else Broj1.Text = (broj_drugi).ToString();
                Broj2.Text = "0";
                zadnja_funk = "*";
            }
        }

        private void Podjeli_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
            }
            else
            {
                double.TryParse(Broj1.Text, out broj_prvi);
                double.TryParse(Broj2.Text, out broj_drugi);
                if(broj_drugi != 0)
                {
                    if (broj_prvi != 0)
                    {
                        Broj1.Text = (broj_prvi / broj_drugi).ToString();
                    }
                    else Broj1.Text = (broj_drugi).ToString();
                Broj2.Text = "0";
                zadnja_funk = "/";
                }
                else
                {
                    MessageBox.Show("Pogrešan unos", "Pogreška!");
                }

            }
        }

        private void Funk_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Sin_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(Broj1.Text, out broj_prvi);
            double.TryParse(Broj2.Text, out broj_drugi);
            Broj1.Text = Math.Sin(broj_drugi).ToString();
            Broj2.Text = "0";
        }

        private void Cos_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(Broj1.Text, out broj_prvi);
            double.TryParse(Broj2.Text, out broj_drugi);
            Broj1.Text = Math.Cos(broj_drugi).ToString();
            Broj2.Text = "0";
        }

        private void Tang_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(Broj1.Text, out broj_prvi);
            double.TryParse(Broj2.Text, out broj_drugi);
            Broj1.Text = Math.Tan(broj_drugi).ToString();
            Broj2.Text = "0";
        }

        private void Cotan_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(Broj1.Text, out broj_prvi);
            double.TryParse(Broj2.Text, out broj_drugi);
            Broj1.Text = (1/Math.Tan(broj_drugi)).ToString();
            Broj2.Text = "0";
        }

        private void b0_Click(object sender, RoutedEventArgs e)
        {
            if(Broj2.Text == "0")Broj2.Text = "";
            Broj2.Text += "0";
        }

        private void b3_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "3";
        }

        private void b2_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "2";
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "1";
        }

        private void b6_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "6";
        }

        private void b5_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "5";
        }

        private void b4_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "4";
        }

        private void b9_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "9";
        }

        private void b8_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "0") Broj2.Text = "";
            Broj2.Text += "8";
        }

        private void b7_Click(object sender, RoutedEventArgs e)
        {
            if (Broj2.Text == "7") Broj2.Text = "";
            Broj2.Text += "3";
        }

        private void Izracunaj_Click(object sender, RoutedEventArgs e)
        {
            switch (zadnja_funk)
            {
                case "+":
                    if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
                    {
                        MessageBox.Show("Pogrešan unos", "Pogreška!");
                    }
                    else
                    {
                        double.TryParse(Broj1.Text, out broj_prvi);
                        double.TryParse(Broj2.Text, out broj_drugi);
                        Broj1.Text = (broj_prvi + broj_drugi).ToString();
                        Broj2.Text = "0";
                        zadnja_funk = "nema funkcije";
                    }
                    funk.Text = "";
                    break;
                case "-":
                    if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
                    {
                        MessageBox.Show("Pogrešan unos", "Pogreška!");
                    }
                    else
                    {
                        double.TryParse(Broj1.Text, out broj_prvi);
                        double.TryParse(Broj2.Text, out broj_drugi);
                        if (broj_prvi != 0)
                        {
                            Broj1.Text = (broj_prvi - broj_drugi).ToString();
                        }
                        else Broj1.Text = (broj_drugi).ToString();

                        Broj2.Text = "0";
                        zadnja_funk = "-";
                    }
                    funk.Text = "";
                    break;
                case "/":
                    if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
                    {
                        MessageBox.Show("Pogrešan unos", "Pogreška!");
                    }
                    else
                    {
                        double.TryParse(Broj1.Text, out broj_prvi);
                        double.TryParse(Broj2.Text, out broj_drugi);
                        if (broj_drugi != 0)
                        {
                            if (broj_prvi != 0)
                            {
                                Broj1.Text = (broj_prvi / broj_drugi).ToString();
                            }
                            else Broj1.Text = (broj_drugi).ToString();
                            Broj2.Text = "0";
                            zadnja_funk = "/";
                        }
                        else
                        {
                            MessageBox.Show("Pogrešan unos", "Pogreška!");
                        }
                    }
                    funk.Text = "";
                    break;
                case "*":
                    if (!double.TryParse(Broj2.Text, out broj_prvi) && !double.TryParse(Broj1.Text, out broj_drugi))
                    {
                        MessageBox.Show("Pogrešan unos", "Pogreška!");
                    }
                    else
                    {
                        double.TryParse(Broj1.Text, out broj_prvi);
                        double.TryParse(Broj2.Text, out broj_drugi);
                        
                            if (broj_prvi != 0)
                            {
                                Broj1.Text = (broj_prvi*broj_drugi).ToString();
                            }
                            else Broj1.Text = (broj_drugi).ToString();
                            Broj2.Text = "0";
                            zadnja_funk = "/";
                        
                    }
                    funk.Text = "";
                    break;
            }
        }

        private void Zbroji_Click(object sender, RoutedEventArgs e)
        {
           
            if (!double.TryParse(Broj2.Text, out broj_prvi)&& !double.TryParse(Broj1.Text, out broj_drugi))
            {
                MessageBox.Show("Pogrešan unos", "Pogreška!");
            }
            else
            {
                double.TryParse(Broj1.Text, out broj_prvi);
                double.TryParse(Broj2.Text, out broj_drugi);
                Broj1.Text = (broj_prvi+broj_drugi).ToString();
                Broj2.Text = "0";
                zadnja_funk = "+";
            }
            funk.Text = "+";
        }
    }
}
